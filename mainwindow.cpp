#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ab_form = new AboutForm();
    uThread = new UniqueThread();
    mutex = new QMutex();

    existing_codes_file = QString("");
    output_file = QString("/Users/nicholasjackson/Documents/c++/Qt/unique_code_gen/build-unique_code_gen-Desktop_Qt_5_1_1_clang_64bit-Debug/test.csv");

    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(on_about_clicked()));
    connect(uThread, SIGNAL(finished()), this, SLOT(on_thread_complete()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ec_browse_clicked()
{
    existing_codes_file = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
    ui->ec_line->setText(existing_codes_file);

}

void MainWindow::on_of_browse_clicked()
{
    output_file = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.csv)"));
    ui->of_line->setText(output_file);

}

void MainWindow::on_gen_button_clicked()
{
    clean_input();

    string code_num = ui->cn_line->text().toStdString();
    string code_length = ui->cl_line->text().toStdString();
    string character_set = ui->cs_line->text().toStdString();

    code_gen = new unique_code_gen(ui->output_text, atoi(code_num.c_str()), atoi(code_length.c_str()), ui->we_check->isChecked(), output_file.toStdString(), character_set, existing_codes_file.toStdString());

    uThread->register_code_gen(code_gen, mutex);
    uThread->start();

    ui->output_text->append(QString("Generating Codes."));
}

bool MainWindow::clean_input()
{
    if(ui->cn_line->text().toStdString() == "")
        return false;

    if(ui->cl_line->text().toStdString() == "")
        return false;

    if(ui->cs_line->text().toStdString() == "")
        return false;

    if(ui->of_line->text().toStdString() == "")
        return false;

    QString code_num = ui->cn_line->text();
    QString code_length = ui->cl_line->text();

    bool rc;
    int rc_i = code_num.toInt(&rc);

    if(!rc)
        return false;

    rc_i = code_length.toInt(&rc);

    if(!rc)
        return false;

    return true;
}

void MainWindow::on_about_clicked()
{
    ab_form->show();
}

void MainWindow::on_thread_complete()
{
    ui->output_text->append(QString("Done."));
}

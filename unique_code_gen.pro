#-------------------------------------------------
#
# Project created by QtCreator 2013-11-07T15:06:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = unique_code_gen
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    unique_code_gen.cpp \
    aboutform.cpp \
    uniquethread.cpp

HEADERS  += mainwindow.h \
    unique_code_gen.h \
    aboutform.h \
    uniquethread.h

FORMS    += mainwindow.ui \
    aboutform.ui

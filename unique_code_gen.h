//
//  unique_code_gen.h
//  unique-code
//
//  Created by nicholas jackson on 28/10/2013.
//  Copyright (c) 2013 nicholas jackson. All rights reserved.
//

#ifndef UNIQUE_CODE_GEN_H
#define UNIQUE_CODE_GEN_H

#include <QTextEdit>


#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <cmath>
#include <fstream>
#include <ctime>

using namespace std;

class unique_code_gen
{
public:
    //constructor
    unique_code_gen(QTextEdit * output, size_t num_codes, size_t code_length, bool write_existing, const string output_file = "", const string charset = "", const string & existing = "");

    //destructor
    ~unique_code_gen(){};

    // main generation function
    bool generate();

private:

    // defined character set
    string charset;
    // defined output file
    string output_file;
    // set of unique codes
    set<string> m_codes;
    // existing codes
    set<string> m_existing_codes;
    // number of required codes
    size_t num_codes;
    // length of codes
    size_t code_length;
    // write existing codes
    bool write_existing;
    // output
    QTextEdit * output;


    // generate random code
    string generate_code(size_t length);
    // check all variables
    bool check_settings();
    // write codes to defined file.
    void write_file();

    bool load_existing_codes(const string & existing);

};

#endif // UNIQUE_CODE_GEN_H


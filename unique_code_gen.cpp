#include "unique_code_gen.h"

using namespace std;

unique_code_gen::unique_code_gen(QTextEdit * output, size_t num_codes, size_t code_length, bool write_existing, const string output_file, const string charset, const string & existing)
{

    //check and write character set
    if(charset == "")
    {
        this->charset = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    }
    else
    {
        this->charset = charset;
    }

    //check and write output file
    if(output_file == "")
    {
        this->output_file = "out.csv";
    }
    else
    {
        this->output_file = output_file;
    }

    //set number of codes and code length
    this->num_codes = num_codes;
    this->code_length = code_length;
    this->write_existing = write_existing;
    this->output = output;

    if(existing != "")
    {
        if(this->load_existing_codes(existing))
            this->num_codes += m_existing_codes.size();
    }
}

string unique_code_gen::generate_code(size_t length)
{
    string code = "";
    string charset = this->charset;

    for(size_t i = 0; i < length; ++i)
    {
        code += charset.substr((rand() % charset.length()), 1);
    }

    return code;
}

bool unique_code_gen::generate()
{
    if(!check_settings())
    {
        cout << "error" << endl;
        return false;
    }

    while(m_codes.size() < num_codes)
    {
        m_codes.insert(generate_code(code_length));
    }

    write_file();

    return true;
}

bool unique_code_gen::check_settings()
{
    if(charset == "")
    {
        stringstream err;
        err << "Charset error.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    if(output_file == "")
    {
        stringstream err;
        err << "output file error.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    if(num_codes == 0)
    {
        stringstream err;
        err << "num codes error.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    if(code_length == 0)
    {
        stringstream err;
        err << "code length error.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    if(num_codes > pow((double)charset.size(), (double)code_length))
    {
        stringstream err;
        err << "Not enough possible combinations.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    return true;
}

void unique_code_gen::write_file()
{
    fstream out;
    out.open(output_file.c_str(), fstream::out);
    set<string>::iterator iter;
    set<string>::iterator iter2;

    if(!out.is_open())
    {
        stringstream err;
        err << "could not open. \"" << output_file << "\"";
        this->output->append(QString(err.str().c_str()));
    }

    this->output->append(QString("Writing to file."));

    for(iter = m_codes.begin(); iter != m_codes.end(); iter++)
    {
        if(!this->write_existing)
        {
            iter2 = m_existing_codes.find(*iter);

            if(iter2 == m_existing_codes.end())
            {
                out << "\"" << *iter << "\"" << endl;
            }
        }
        else
        {
            out << "\"" << *iter << "\"" << endl;
        }
    }

    out.close();
}

bool unique_code_gen::load_existing_codes(const string & existing)
{
    ifstream in(existing.c_str());

    if(!in.is_open())
    {
        stringstream err;
        err << "Failed to read existing codes.";
        this->output->append(QString(err.str().c_str()));
        return false;
    }

    while(!in.eof())
    {
        string code = "";
        getline(in, code);

        if(code == "")
            continue;

        code = code.substr(1, code.length()-2);

        m_existing_codes.insert(code);
        m_codes.insert(code);
    }

    return true;
}

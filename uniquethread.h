#ifndef UNIQUETHREAD_H
#define UNIQUETHREAD_H

#include <QThread>
#include <QMutex>
#include "unique_code_gen.h"

class UniqueThread : public QThread
{
    Q_OBJECT
public:
    explicit UniqueThread(QObject *parent = 0);

    void register_code_gen(unique_code_gen * code_gen, QMutex * mutex);

protected:

    void run();

private:

    QMutex * mutex;
    unique_code_gen * code_gen;

signals:

public slots:

};

#endif // UNIQUETHREAD_H

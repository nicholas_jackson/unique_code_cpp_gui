#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>

#include "unique_code_gen.h"
#include "uniquethread.h"
#include "aboutform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_ec_browse_clicked();
    void on_of_browse_clicked();
    void on_gen_button_clicked();
    void on_about_clicked();
    void on_thread_complete();

private:
    Ui::MainWindow *ui;
    AboutForm * ab_form;
    unique_code_gen * code_gen;
    UniqueThread * uThread;
    QMutex * mutex;

    bool clean_input();

    QString output_file;
    QString existing_codes_file;
};

#endif // MAINWINDOW_H

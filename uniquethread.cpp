#include "uniquethread.h"

UniqueThread::UniqueThread(QObject *parent) :
    QThread(parent)
{
}

void UniqueThread::register_code_gen(unique_code_gen *code_gen, QMutex * mutex)
{
    this->code_gen = code_gen;
    this->mutex = mutex;
}

void UniqueThread::run()
{
    mutex->lock();
    code_gen->generate();
    mutex->unlock();
}
